import React from 'react';
import { Avatar, List } from 'antd';

export default ({ friends }) => {
    return (
        <List
            header="People I know"
            itemLayout="horizontal"
            dataSource={friends}
            renderItem={item => (
                <List.Item>
                    <List.Item.Meta
                        avatar={
                            item.imageSrc ? (
                                <Avatar src={item.imageSrc} />
                            ) : (
                                <Avatar icon="user" />
                            )
                        }
                        title={<a href={item.webId}>{item.name}</a>}
                        description={
                            <a href={item.homepage}>{item.homepage}</a>
                        }
                    />
                </List.Item>
            )}
        />
    );
};
