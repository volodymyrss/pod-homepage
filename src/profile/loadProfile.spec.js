import { loadProfile } from './loadProfile';
import data from '@solid/query-ldflex';
import { value } from '../test-utils/ldflex';

jest.mock('@solid/query-ldflex', () => ({}));

describe('when loading a profile', () => {
    describe('containing no relevant data successfully', () => {
        let profile;

        beforeEach(async () => {
            data['https://pod.example/jane#me'] = {};
            profile = await loadProfile('https://pod.example/jane#me');
        });

        it('then the result contains the webId', () => {
            expect(profile.webId).toBe('https://pod.example/jane#me');
        });

        it('and the webId is used as a fallback name', () => {
            expect(profile.name).toBe('https://pod.example/jane#me');
        });

        it('and the image src is undefined', () => {
            expect(profile.imageSrc).toBeUndefined();
        });

        it('and the role is undefined', () => {
            expect(profile.role).toBeUndefined();
        });

        it('and the organization is undefined', () => {
            expect(profile.organization).toBeUndefined();
        });

        it('and the homepage is undefined', () => {
            expect(profile.homepage).toBeUndefined();
        });
    });

    describe('containing basic data successfully', () => {
        let profile;

        beforeEach(async () => {
            data['https://pod.example/jane#me'] = {
                name: value('Jane Doe'),
                image: value('https://pod.example/jane.png'),
                vcard_role: value('Test double'),
                homepage: value('https://homepage.example/'),
                'vcard:organization-name': value('Solid Community'),
            };
            profile = await loadProfile('https://pod.example/jane#me');
        });

        describe('then the result contains', () => {
            it('the webId', () => {
                expect(profile.webId).toBe('https://pod.example/jane#me');
            });

            it('the name', () => {
                expect(profile.name).toBe('Jane Doe');
            });

            it('the image src', () => {
                expect(profile.imageSrc).toBe('https://pod.example/jane.png');
            });

            it('the role', () => {
                expect(profile.role).toBe('Test double');
            });

            it('the organization', () => {
                expect(profile.organization).toBe('Solid Community');
            });

            it('the homepage', () => {
                expect(profile.homepage).toBe('https://homepage.example/');
            });
        });
    });

    describe('with a vcard photo', () => {
        let profile;

        beforeEach(async () => {
            data['https://pod.example/jane#me'] = {
                vcard_hasPhoto: value('https://pod.example/jane.png'),
            };
            profile = await loadProfile('https://pod.example/jane#me');
        });

        it('the result contains the image src', () => {
            expect(profile.imageSrc).toBe('https://pod.example/jane.png');
        });
    });
});
