import { useEffect, useState } from 'react';
import { loadProfile } from './loadProfile';
import { loadAddress } from './loadAddress';

export const useProfile = webId => {
    const [profile, setProfile] = useState({});
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(false);

    useEffect(() => {
        const profile = loadProfile(webId);
        const address = loadAddress(webId);

        Promise.all([profile, address])
            .then(([profile, address]) => {
                setProfile({
                    ...profile,
                    ...address,
                });
                setLoading(false);
            })
            .catch(err => {
                setError(err);
                setLoading(false);
            });
    }, [webId]);

    return {
        loading,
        error,
        ...profile,
    };
};
