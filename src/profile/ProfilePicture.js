import { Avatar } from 'antd';
import React from 'react';

export const ProfilePicture = ({ src }) => (
    <Avatar size={150} src={src} icon="user" />
);
