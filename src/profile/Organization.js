import { Icon } from 'antd';
import React from 'react';

export const Organization = ({ name }) => {
    if (!name) return null;
    return (
        <>
            <Icon type="laptop"/> {name}
        </>
    );
};
