import data from '@solid/query-ldflex';

export const loadBookmarks = async documentUri => {
    const bookmarks = data[documentUri].dct_references;
    const result = [];
    for await (const bookmark of bookmarks) {
        const uri = bookmark.toString();
        const title = await data[uri].dct_title;
        const recalls = await data[uri][
            'http://www.w3.org/2002/01/bookmark#recalls'
        ];
        const created = await data[uri].dct_created;
        if (recalls) {
            result.push({
                title: title ? `${title}` : `${recalls}`,
                recalls: `${recalls}`,
                created: created && new Date(created),
            });
        }
    }
    return result;
};
