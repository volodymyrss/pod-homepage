import data from '@solid/query-ldflex';
import { emptyList, value, list } from '../test-utils/ldflex';
import { loadBookmarks } from './loadBookmarks';

jest.mock('@solid/query-ldflex', () => ({}));

describe('when loading bookmarks', () => {
    describe('from an empty document', () => {
        let result;
        beforeEach(async () => {
            data['https://pod.example/bookmarks'] = {
                dct_references: emptyList(),
            };
            result = await loadBookmarks('https://pod.example/bookmarks');
        });

        it('the result is an empty array', () => {
            expect(result).toEqual([]);
        });
    });

    describe('from a document referring to one bookmark', () => {
        let result;
        beforeEach(async () => {
            data['https://pod.example/bookmarks'] = {
                dct_references: list(['https://pod.example/bookmarks#1']),
            };
            data['https://pod.example/bookmarks#1'] = {
                dct_title: value('A bookmark'),
                'http://www.w3.org/2002/01/bookmark#recalls': value(
                    'https://site.example/'
                ),
                dct_created: value('2019-05-23T12:43:10.459Z'),
            };
            result = await loadBookmarks('https://pod.example/bookmarks');
        });

        it('the result contains 1 bookmark', () => {
            expect(result).toHaveLength(1);
        });

        it('the bookmark contains the title', () => {
            expect(result[0].title).toBe('A bookmark');
        });

        it('the bookmark contains the url it recalls', () => {
            expect(result[0].recalls).toBe('https://site.example/');
        });

        it('the bookmark contains the creation date', () => {
            expect(result[0].created).toEqual(
                new Date('2019-05-23T12:43:10.459Z')
            );
        });
    });

    describe('from a document referring to one bookmark with only recalls', () => {
        let result;
        beforeEach(async () => {
            data['https://pod.example/bookmarks'] = {
                dct_references: list(['https://pod.example/bookmarks#1']),
            };
            data['https://pod.example/bookmarks#1'] = {
                'http://www.w3.org/2002/01/bookmark#recalls': value(
                    'https://site.example/'
                ),
            };
            result = await loadBookmarks('https://pod.example/bookmarks');
        });

        it('the result contains 1 bookmark', () => {
            expect(result).toHaveLength(1);
        });

        it('the bookmark uses the recalls as title', () => {
            expect(result[0].title).toBe('https://site.example/');
        });

        it('the bookmark contains the url it recalls', () => {
            expect(result[0].recalls).toBe('https://site.example/');
        });

        it('the bookmark has no creation date', () => {
            expect(result[0].created).toBeUndefined();
        });
    });

    describe('from a document referring to three bookmarks with recalls and two without', () => {
        let result;
        beforeEach(async () => {
            data['https://pod.example/bookmarks'] = {
                dct_references: list([
                    'https://pod.example/bookmark#1',
                    'https://pod.example/bookmark#2',
                    'https://pod.example/bookmark#3',
                    'https://pod.example/bookmark#4',
                    'https://pod.example/bookmark#5',
                ]),
            };
            data['https://pod.example/bookmark#1'] = {};
            data['https://pod.example/bookmark#2'] = {
                'http://www.w3.org/2002/01/bookmark#recalls': value(
                    'https://site1.example/'
                ),
            };
            data['https://pod.example/bookmark#3'] = {};
            data['https://pod.example/bookmark#4'] = {
                'http://www.w3.org/2002/01/bookmark#recalls': value(
                    'https://site2.example/'
                ),
            };
            data['https://pod.example/bookmark#5'] = {
                'http://www.w3.org/2002/01/bookmark#recalls': value(
                    'https://site3.example/'
                ),
            };

            result = await loadBookmarks('https://pod.example/bookmarks');
        });

        it('the result contains only the 3 bookmarks with a recalls', () => {
            expect(result).toHaveLength(3);
            const recalls = result.map(it => it.recalls);
            expect(recalls).toContain('https://site1.example/');
            expect(recalls).toContain('https://site2.example/');
            expect(recalls).toContain('https://site3.example/');
        });
    });
});
