import data from '@solid/query-ldflex';

export const loadTypeRegistration = async (index, classIri) => {
    const registrations = data[index].dct_references;
    for await (const registration of registrations) {
        const instance = await data[registration].solid_instance;
        const forClass = await data[registration].solid_forClass;
        if (`${forClass}` === classIri) {
            return instance && `${instance}`;
        }
    }
};