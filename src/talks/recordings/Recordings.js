import React from 'react';
import { Recording } from './Recording';

export const Recordings = ({ talkTitle, list }) => {
    return list.map(file => (
        <div key={file.uri}>
            <Recording talkTitle={talkTitle} {...file} />
        </div>
    ));
};
