import { Button, Modal } from 'antd';
import React from 'react';
import { Video } from './Video';

function play({ talkTitle, name, contentUrl, embedUrl, mimeType }) {
    Modal.info({
        title: talkTitle,
        icon: 'video-camera',
        width: 800,
        content: (
            <Video
                name={name}
                contentUrl={contentUrl}
                embedUrl={embedUrl}
                mimeType={mimeType}
            />
        ),
        onOk() {},
    });
}

export const PlayVideo = recording => (
    <Button type="primary" icon="caret-right" onClick={() => play(recording)}>
        Play
    </Button>
);
