import React from 'react';
import { Recording } from './Recording';
import { PlayVideo } from './PlayVideo';
import { shallow } from 'enzyme';
import { Download } from './Download';

describe('A Recording', () => {
    describe('with content url', () => {
        let result;
        beforeEach(() => {
            result = shallow(
                <Recording contentUrl="https://content.example" />
            );
        });

        it('can be played', () => {
            const play = result.find(PlayVideo);
            expect(play).toExist();
            expect(play).toHaveProp('contentUrl', 'https://content.example');
        });
        it('can be downloaded', () => {
            const download = result.find(Download);
            expect(download).toExist();
            expect(download).toHaveProp('url', 'https://content.example');

        });
    });
    describe('with embed url only', () => {
        let result;
        beforeEach(() => {
            result = shallow(
                <Recording embedUrl="https://content.example" />
            );
        });
        it('can be played', () => {
            const play = result.find(PlayVideo);
            expect(play).toExist();
            expect(play).toHaveProp('embedUrl', 'https://content.example');
        });
        it('can not be downloaded', () => {
            const download = result.find(Download);
            expect(download).not.toExist();

        });
    });
});
