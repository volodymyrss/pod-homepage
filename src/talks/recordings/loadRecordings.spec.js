import data from '@solid/query-ldflex';
import { emptyList, list, value } from '../../test-utils/ldflex';
import { loadRecordings } from './loadRecordings';
import { loadMedia } from '../media/loadMedia';

jest.mock('@solid/query-ldflex', () => ({}));
jest.mock('../media/loadMedia');

describe('when loading recordings of a talk', () => {

    beforeEach(() => {
        jest.resetAllMocks();
    });
    describe('that does not link to any', () => {
        let slides;
        beforeEach(async () => {
            data['https://person.example/talks/1#it'] = {
                schema_recordedIn: emptyList(),
            };
            slides = await loadRecordings('https://person.example/talks/1#it');
        });
        it('returns an empty list', () => {
            expect(slides).toEqual([]);
        });
    });

    describe('that does links to one recording', () => {
        let recordings;
        beforeEach(async () => {
            data['https://person.example/talks/1#it'] = {
                schema_recordedIn: list([
                    'https://person.example/talks/1#recording',
                ]),
            };
            loadMedia.mockResolvedValue({
                uri: 'https://person.example/talks/1#recording',
                type: 'video/mp4',
                name: 'recording.mp4',
            });
            recordings = await loadRecordings(
                'https://person.example/talks/1#it'
            );
        });
        it('returns a list with one recording file', () => {
            expect(recordings).toHaveLength(1);
        });

        it('should load recording media', () => {
            expect(loadMedia).toHaveBeenCalledWith(
                'https://person.example/talks/1#recording'
            );
        });

        it('should return the loaded recording media', () => {
            expect(recordings[0]).toEqual({
                uri: 'https://person.example/talks/1#recording',
                type: 'video/mp4',
                name: 'recording.mp4',
            });
        });
    });

    describe('that does links to three recordings', () => {
        let recordings;
        beforeEach(async () => {
            data['https://person.example/talks/1#it'] = {
                schema_recordedIn: list([
                    'https://person.example/talks/1#recording1',
                    'https://person.example/talks/1#recording2',
                    'https://person.example/talks/1#recording3',
                ]),
            };
            recordings = await loadRecordings(
                'https://person.example/talks/1#it'
            );
        });
        it('returns a list with three recording files', () => {
            expect(recordings).toHaveLength(3);
        });

        it('should load recording media for all three recordings', () => {
            expect(loadMedia).toHaveBeenCalledWith(
                'https://person.example/talks/1#recording1'
            );
            expect(loadMedia).toHaveBeenCalledWith(
                'https://person.example/talks/1#recording2'
            );
            expect(loadMedia).toHaveBeenCalledWith(
                'https://person.example/talks/1#recording3'
            );
        });
    });
});
