import data from '@solid/query-ldflex';
import { loadSlides } from './slides/loadSlides';
import { loadRecordings } from './recordings/loadRecordings';

export const loadTalk = async uri => {
    const title = await data[uri].schema_name;
    const description = await data[uri].schema_description;
    const startDate = await data[uri].schema_startDate;
    const slides = await loadSlides(uri);
    const recordings = await loadRecordings(uri);
    return {
        uri,
        title: title ? title.toString() : uri,
        description: description && description.toString(),
        date: startDate && new Date(startDate),
        slides,
        recordings,
    };
};