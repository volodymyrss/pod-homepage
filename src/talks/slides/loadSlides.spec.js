import { loadSlides } from './loadSlides';
import data from '@solid/query-ldflex';
import { list, value } from '../../test-utils/ldflex';
import { loadMedia } from '../media/loadMedia';

jest.mock('@solid/query-ldflex', () => ({}));
jest.mock('../media/loadMedia');

describe('when loading slides of a talk', () => {
    beforeEach(() => {
        jest.resetAllMocks();
    });

    describe('that does not link to any', () => {
        let slides;
        beforeEach(async () => {
            data['https://person.example/talks/1#it'] = {};
            slides = await loadSlides('https://person.example/talks/1#it');
        });
        it('returns an empty list', () => {
            expect(slides).toEqual([]);
        });
    });

    describe('that does links to slides with one associated media file', () => {
        let slides;
        beforeEach(async () => {
            data['https://person.example/talks/1#it'] = {
                schema_workFeatured: value(
                    'https://person.example/talks/1#slides'
                ),
            };
            data['https://person.example/talks/1#slides'] = {
                schema_associatedMedia: list([
                    'https://person.example/talks/1#pdf',
                ]),
            };
            loadMedia.mockResolvedValue({
                uri: 'https://person.example/talks/1#pdf',
                type: 'application/pdf',
                name: 'slides.pdf',
            });
            slides = await loadSlides('https://person.example/talks/1#it');
        });
        it('returns a list with one slide file', () => {
            expect(slides).toHaveLength(1);
        });

        it('should load slides media', () => {
            expect(loadMedia).toHaveBeenCalledWith(
                'https://person.example/talks/1#pdf'
            );
        });

        it('should return the loaded slide media', () => {
            expect(slides[0]).toEqual({
                uri: 'https://person.example/talks/1#pdf',
                type: 'application/pdf',
                name: 'slides.pdf',
            });
        });
    });

    describe('that does links to slides with three associated media file', () => {
        let slides;
        beforeEach(async () => {
            data['https://person.example/talks/1#it'] = {
                schema_workFeatured: value(
                    'https://person.example/talks/1#slides'
                ),
            };
            data['https://person.example/talks/1#slides'] = {
                schema_associatedMedia: list([
                    'https://person.example/talks/1#pdf',
                    'https://person.example/talks/1#web',
                    'https://person.example/talks/1#other',
                ]),
            };
            slides = await loadSlides('https://person.example/talks/1#it');
        });
        it('returns a list with three slide files', () => {
            expect(slides).toHaveLength(3);
        });

        it('should load slide media for all three slides', () => {
            expect(loadMedia).toHaveBeenCalledWith(
                'https://person.example/talks/1#pdf'
            );
            expect(loadMedia).toHaveBeenCalledWith(
                'https://person.example/talks/1#web'
            );
            expect(loadMedia).toHaveBeenCalledWith(
                'https://person.example/talks/1#other'
            );
        });
    });
});
