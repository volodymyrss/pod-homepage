import data from '@solid/query-ldflex';
import { value } from '../../test-utils/ldflex';
import { loadMedia } from './loadMedia';

jest.mock('@solid/query-ldflex', () => ({}));
describe('when loading media', () => {
    describe('with all relevant data', () => {
        let media;
        beforeEach(async () => {
            data['https://person.example/slides/1#pdf'] = {
                schema_name: value('slides.pdf'),
                schema_description: value('Slides of my talk'),
                schema_encodingFormat: value('application/pdf'),
                schema_contentUrl: value('https://person.example/slides/1.pdf'),
            };
            media = await loadMedia('https://person.example/slides/1#pdf');
        });

        it('should contain the uri', () => {
            expect(media.uri).toBe('https://person.example/slides/1#pdf');
        });

        it('should contain the name', () => {
            expect(media.name).toBe('slides.pdf');
        });

        it('should contain the description', () => {
            expect(media.description).toBe('Slides of my talk');
        });

        it('should contain the mime type', () => {
            expect(media.mimeType).toBe('application/pdf');
        });

        it('should contain the content url', () => {
            expect(media.contentUrl).toBe(
                'https://person.example/slides/1.pdf'
            );
        });
    });

    describe('with only a content url', () => {
        let media;
        beforeEach(async () => {
            data['https://person.example/slides/1#pdf'] = {
                schema_contentUrl: value('https://person.example/slides/1.pdf'),
            };
            media = await loadMedia('https://person.example/slides/1#pdf');
        });

        it('should contain the uri', () => {
            expect(media.uri).toBe('https://person.example/slides/1#pdf');
        });

        it('should use content url as name', () => {
            expect(media.name).toBe('https://person.example/slides/1.pdf');
        });

        it('should not contain a description', () => {
            expect(media.description).toBeUndefined();
        });

        it('should not contain a mime type', () => {
            expect(media.mimeType).toBeUndefined();
        });

        it('should contain the content url', () => {
            expect(media.contentUrl).toBe(
                'https://person.example/slides/1.pdf'
            );
        });
    });

    describe('with an embed url', () => {
        let media;
        beforeEach(async () => {
            data['https://person.example/recording#1'] = {
                schema_embedUrl: value('https://person.example/recording/embed'),
            };
            media = await loadMedia('https://person.example/recording#1');
        });

        it('should contain an embed url', () => {
            expect(media.embedUrl).toBe('https://person.example/recording/embed');
        });

    });

    describe('without any data', () => {
        let media;
        beforeEach(async () => {
            data['https://person.example/slides/1#pdf'] = {};
            media = await loadMedia('https://person.example/slides/1#pdf');
        });

        it('should contain the uri', () => {
            expect(media.uri).toBe('https://person.example/slides/1#pdf');
        });

        it('should use uri as name', () => {
            expect(media.name).toBe('https://person.example/slides/1#pdf');
        });

        it('should not contain a description', () => {
            expect(media.description).toBeUndefined();
        });

        it('should not contain a mime type', () => {
            expect(media.mimeType).toBeUndefined();
        });

        it('should not contain a content url', () => {
            expect(media.contentUrl).toBeUndefined();
        });

        it('should not contain a embed url', () => {
            expect(media.embedUrl).toBeUndefined();
        });
    });
});
