import { useBlogPosts } from './useBlogPosts';
import React from 'react';
import { List } from 'antd';

export default ({ webId }) => {
    const { posts, loading } = useBlogPosts(webId);
    return (
        <List
            header="From my blogs:"
            itemLayout="vertical"
            loading={loading}
            dataSource={posts}
            renderItem={item => (
                <List.Item>
                    <List.Item.Meta
                        title={<a href={item.uri}>{item.title}</a>}
                        description={
                            <span>
                                {item.created
                                    ? item.created.toLocaleDateString()
                                    : null}{' '}
                                |{' '}
                                <a href={item.blog.homepage}>
                                    {item.blog.name}
                                </a>
                            </span>
                        }
                    />
                </List.Item>
            )}
        />
    );
};
