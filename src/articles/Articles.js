import React from 'react';
import BlogPosts from './BlogPosts';

export default ({ webId }) => <BlogPosts webId={webId} />;
